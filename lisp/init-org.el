;;; init-org.el --- Org-mode config -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package org
  ;; Use built-in org mode
  :straight (org :type built-in)
  :bind
  (:map org-mode-map
        ("C-M-<up>" . org-up-element))
  :hook
  (org-mode . toggle-word-wrap)
  :custom
  (org-startup-truncated nil)
  (org-log-done t)
  (org-edit-timestamp-down-means-later t)
  (org-hide-emphasis-markers t)
  (org-catch-invisible-edits 'show-and-error)
  (org-export-coding-system 'utf-8)
  (org-fast-tag-selection-single-key 'expert)
  (org-html-validation-link nil)
  (org-export-kill-product-buffer-when-displayed t)
  (org-tags-column 0)
  (org-auto-align-tags nil)
  (org-special-ctrl-a/e t)
  (org-insert-heading-respect-content t)
  (org-pretty-entities t)
  (org-ellipsis "…")
  (org-support-shift-select t)
  (org-todo-keywords
      (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!/!)")
              (sequence "PROJECT(p)" "|" "DONE(d!/!)" "CANCELLED(c@/!)")
              (sequence "WAITING(w@/!)" "DELEGATED(e!)" "HOLD(h)" "|" "CANCELLED(c@/!)"))))
  (org-todo-repeat-to-state "NEXT")
  (org-log-into-drawer t)
  :config
  (org-clock-persistence-insinuate)
  (when (executable-find "soffice")
    (setq org-odt-preferred-output-format "docx"))
  (org-babel-do-load-languages
   'org-babel-load-languages
   (seq-filter
    (lambda (pair)
      (featurep (intern (concat "ob-" (symbol-name (car pair))))))
    '((R . t)
      (ditaa . t)
      (dot . t)
      (emacs-lisp . t)
      (gnuplot . t)
      (haskell . nil)
      (latex . t)
      (ledger . t)
      (ocaml . nil)
      (octave . t)
      (plantuml . t)
      (python . t)
      (ruby . t)
      (screen . nil)
      (shell . t)
      (sql . t)
      (sqlite . t)))))

;; org-contrib
(use-package org-contrib
  :after org
  :config
  (require 'ox-bibtex))

;; org-agenda
(use-package org-agenda
  :straight nil
  :after org
  :custom
  (org-agenda-time-grid
   '((daily today require-timed)
     (800 1000 1200 1400 1600 1800 2000)
     " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄"))
  (org-agenda-clockreport-parameter-plist '(:link t :maxlevel 3))
  :config
  (add-to-list 'org-agenda-after-show-hook 'org-show-entry))

;; org-cite
(use-package oc
  :straight nil
  :custom
  (org-cite-insert-processor 'citar)
  (org-cite-follow-processor 'citar)
  (org-cite-activate-processor 'citar)
  :config
  (require 'oc-biblatex)
  (require 'oc-natbib)
  (when (file-directory-p "~/Dropbox")
    (setq org-cite-global-bibliography '("~/Dropbox/zotero-bibtex/library.bib" "~/Dropbox/custom-bibtex.bib"))))

;;; Org clock
(use-package org-clock
  :straight nil
  :no-require
  :bind
  (:map org-clock-mode-line-map
        ([header-line mouse-2] . org-clock-goto)
        ([header-line mouse-2] . org-clock-menu))
  :custom
  (org-clock-persist t)
  (org-clock-in-resume t)
  (org-clock-into-drawer t)
  (org-clock-out-remove-zero-time-clocks t)
  :config
  ;; Show clock sums as hours and minutes, not "n days" etc.
  (setq org-time-clocksum-format
        '(:hours "%d" :require-hours t :minutes ":%02d" :require-minutes t))
  ;;; Show the clocked-in task - if any - in the header line
  (defun sanityinc/show-org-clock-in-header-line ()
    (setq-default header-line-format '((" " org-mode-line-string " "))))

  (defun sanityinc/hide-org-clock-from-header-line ()
    (setq-default header-line-format nil))
  (add-hook 'org-clock-in-hook 'sanityinc/show-org-clock-in-header-line)
  (add-hook 'org-clock-out-hook 'sanityinc/hide-org-clock-from-header-line)
  (add-hook 'org-clock-cancel-hook 'sanityinc/hide-org-clock-from-header-line))

;;; Archiving
(use-package org-archive
  :straight nil
  :no-require
  :custom
  (org-archive-mark-done nil)
  (org-archive-location "%s_archive::* Archive"))

;;; org-modern theme
(use-package org-modern
  :after org
  :custom
  (org-modern-todo-faces
   (quote (("NEXT" :inherit warning)
           ("PROJECT" :inherit font-lock-string-face))))
  :hook
  ((org-mode . org-modern-mode)
   (org-agenda-finalize-hook . org-modern-agenda))
  :config
  (require 'org-indent))

;;; Useful packages
(use-package org-cliplink
  :after org)
(use-package org-fragtog
  :after org
  :hook (org-mode . org-fragtog-mode))

;;; Roam
(use-package org-roam
  :if (file-directory-p "~/Documents/org-roam-files")
  :custom
  (org-roam-directory (file-truename "~/Documents/org-roam-files"))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ;; Dailies
         ("C-c n j" . org-roam-dailies-capture-today))
  :config
  (add-to-list 'org-roam-capture-templates
               '("b" "Beamer" plain "%?"
                 :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                                    "#+title: ${title}\n
#+author: Yuji Kanagawa
#+language: en
#+options: H:2 num:t toc:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+options: TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+infojs_opt: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:https://orgmode.org/org-info.js
#+export_select_tags: export
#+export_exclude_tags: noexport
#+startup: beamer
#+beamer_theme: metropolis
#+latex_class: beamer
#+latex_class_options: [bigger]\n")
                 :unnarrowed t))
  (add-to-list 'org-roam-capture-templates
               '("l" "Latex" plain "%?"
                 :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                                    "#+title: ${title}
#+AUTHOR: Yuji Kanagawa
#+EMAIL: yuji.kanagawa@oist.jp
#+LANGUAGE: en
#+OPTIONS: H:3 num:t toc:nil @:t ::t |:t ^:nil -:t f:t *:t <:nil
#+OPTIONS: author:t creator:nil timestamp:t email:t
#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS: [11pt]
#+LATEX_HEADER: \\usepackage{times}
#+LATEX_HEADER: \\usepackage{amssymb}
#+LATEX_HEADER: \\usepackage{amsmath}
#+LATEX_HEADER: \\usepackage{amsfonts}
#+LATEX_HEADER: \\usepackage{bm}
#+LATEX_HEADER: \\usepackage{nicefrac}
#+LATEX_HEADER: \\usepackage{xcolor}
#+LATEX_HEADER: \\definecolor{twilightblue}{HTML}{363b74}
#+LATEX_HEADER: \\definecolor{twilightpink}{HTML}{ef4f91}
#+LATEX_HEADER: \\definecolor{twilightbluepurple}{HTML}{4d1b7b}
#+LATEX_HEADER: \\definecolor{twilightpurple}{HTML}{673888}
#+LATEX_HEADER: \\usepackage{hyperref}
#+LATEX_HEADER: \\usepackage{cleveref}
#+LATEX_HEADER: \\hypersetup{colorlinks=true, citecolor=twilightblue, linkcolor=twilightpink, linktocpage=true, urlcolor=twilightpurple}
#+LATEX_HEADER: \\usepackage{geometry}
#+LATEX_HEADER: \\geometry{left=25mm, right=25mm, top=30mm, bottom=30mm}
#+LATEX_HEADER: \\newcounter{num}
#+LATEX_HEADER: \\newcommand{\\rnum}[1]{\\setcounter{num}{#1}(\\roman{num})}\n")))
  ;; If you're using a vertical completion framework, you might want a more informative completion interface
  (setq org-roam-node-display-template
        (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-db-autosync-mode))

(use-package org-roam-bibtex
  :after org-roam)


;;; Image clipping
;; Max ration of the image to the window size
(defconst boremacs/org-max-image-ratio '((width . 0.8) (height . 0.4)))
;; Only true when called org-display-inline-image
(defvar boremacs/creating-org-inline-image nil)

(defun boremacs/truncate-value (value ratio)
  "Scale VALUE by RATIO."
  (ceiling (* ratio value)))

(defun boremacs/+truncate-image-with-org
    (old-fun file-or-data &optional type data-p &rest props)
  "Truncate image when creating an iamge."
  (if (and boremacs/creating-org-inline-image
           (null type)
           (null (plist-get props :width)))
      (let-alist boremacs/org-max-image-ratio
        (let ((max-width (boremacs/truncate-value (frame-text-width) .width))
              (max-height (boremacs/truncate-value (frame-text-height) .height)))
          (apply old-fun file-or-data
           (if (image-type-available-p 'imagemagick) 'imagemagick)
           data-p
           (plist-put
            (plist-put props :max-width max-width)
            :max-height max-height))))
    (apply old-fun file-or-data type data-p props)))

(defun boremacs/+org-truncate-image-in-inline-image (old-func &rest args)
  "Wrap OLD-FUNC (with ARGS) with boremacs/creating-org-inline-image t."
  (let ((boremacs/creating-org-inline-image t))
    (apply old-func args)))

(advice-add #'create-image :around #'boremacs/+truncate-image-with-org)
(advice-add #'org-display-inline-images :around #'boremacs/+org-truncate-image-in-inline-image)

(provide 'init-org)
;;; init-org.el ends here
