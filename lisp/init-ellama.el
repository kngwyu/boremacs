;;; init-ellama.el ---Ellama support -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

;; ellama
(use-package ellama
  :bind ("C-c e" . ellama-transient-main-menu)
  :init
  (require 'llm-ollama)
  (setopt ellama-provider
   (make-llm-ollama
    :host "localhost"
    :port 11434
    :chat-model "qwen2.5-coder:7b")))

(provide 'init-ellama)
;;; init-ellama.el ends here
