;;; init-spelling.el --- Spell check settings -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'ispell)

(use-package ispell
  :straight (ispell :type built-in)
  :config
  (dolist (block '((":\\(PROPERTIES\\|LOGBOOK\\):" . ":END:")
                   ("#\\+BEGIN_SRC" . "#\\+END_SRC")
                   ("#\\+BEGIN_EXAMPLE" . "#\\+END_EXAMPLE")))
    (add-to-list 'ispell-skip-region-alist block))
  (when (boremacs/executable-find "hunspell")
    (setq ispell-program-name "hunspell")))

(use-package flyspell
  :straight (flyspell :type built-in)
  :diminish
  :hook
  ((org-mode
    markdown-mode
    LaTeX-mode
    rst-mode
    git-commit-mode) . flyspell-mode)
  :custom
  (flyspell-issue-welcome-flag nil)
  (flyspell-issue-message-flag nil)
  :config
  ;; I use this for embark-act
  (define-key flyspell-mode-map (kbd "C-.") nil))

(use-package flyspell-lazy
  :after flyspell
  :hook
  (flyspell-mode . flyspell-lazy-mode)
  :custom
  (flyspell-lazy-idle-seconds 1)
  (flyspell-lazy-window-idle-seconds 3))

(use-package consult-flyspell
  :bind
  ("M-g s" . consult-flymake))

(provide 'init-spelling)
;;; init-spelling.el ends here
