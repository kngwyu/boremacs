;;; init-lua.el --- lua language support -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

;; lua-mode
(use-package lua-mode
  :mode "\\.lua\\'"
  :custom
  (lua-indent-level 2))

(provide 'init-lua)
;;; init-lua.el ends here
