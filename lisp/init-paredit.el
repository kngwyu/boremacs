;;; init-paredit.el --- Configure paredit structured editing -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package paredit
  :diminish paredit-mode
  :hook
  (paredit-mode . sanityinc/maybe-map-paredit-newline)
  :config
  (dolist (keybind '("M-s" "M-/" "M-;" "M-r" "M-?" "M-S" "M-J" "M-q"))
    (define-key paredit-mode-map (kbd keybind) nil))
  (defun sanityinc/maybe-map-paredit-newline ()
    (unless (or (memq major-mode '(inferior-emacs-lisp-mode cider-repl-mode))
                (minibufferp))
      (local-set-key (kbd "RET") 'paredit-newline))))


(provide 'init-paredit)
;;; init-paredit.el ends here
