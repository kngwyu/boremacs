;;; init-javascript.el --- JS&TS editing -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:
(use-package js2-mode
  :custom
  (js-indent-level 2)
  (js2-bounce-indent-p nil)
  (js2-mode-show-parse-errors nil)
  (js2-mode-show-strict-warnings nil)
  :config
  (when (boremacs/executable-find "rg")
    (setq-default xref-js2-search-program 'rg)))

(use-package treesit-auto
  :unless (version< emacs-version "29.0")
  :custom
  (treesit-auto-install 'prompt)
  (treesit-auto-langs '(typescript tsx astro))
  (global-treesit-auto-mode))

(use-package astro-ts-mode
  :unless (version< emacs-version "29.0")
  :mode (("\\.astro\\'" . astro-ts-mode))
  :config
  (let ((astro-recipe (make-treesit-auto-recipe
                       :lang 'astro
                       :ts-mode 'astro-ts-mode
                       :url "https://github.com/virchau13/tree-sitter-astro"
                       :revision "master"
                       :source-dir "src")))
    (add-to-list 'treesit-auto-recipe-list astro-recipe)))

(use-package typescript-ts-mode
  :unless (version< emacs-version "29.0")
  :mode (("\\.ts\\'" . typescript-ts-mode)
         ("\\.tsx\\'" . tsx-ts-mode)))

(provide 'init-javascript)
;;; init-javascript.el ends here
