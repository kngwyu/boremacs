;;; init-recentf.el --- Settings for tracking recent files -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package recentf
  :straight nil
  :init (recentf-mode 1)
  :custom
  (recentf-max-saved-items 200)
  (recentf-exclude `("/tmp/" "/ssh:" ,(concat package-user-dir "/.*-autoloads\\.el\\'")))
  :config
  (setq recentf-auto-cleanup (if (daemonp) 300 nil))
  (add-hook 'kill-emacs-hook #'recentf-cleanup))

(provide 'init-recentf)
;;; init-recentf.el ends here
