;;; init-racket.el --- Racket mode -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package racket-mode
  :mode "\\.rkt\\'"
  :hook ((racket-mode . racket-xp-mode)
         (racket-mode . eglot-ensure)))

(provide 'init-racket)
;;; init-racket.el ends here
