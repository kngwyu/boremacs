;;; init-keychain.el --- Use keychain in Emacs -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package keychain-environment
  :if (executable-find "keychain")
  :init (keychain-refresh-environment))

(provide 'init-keychain)
;;; init-keychain.el ends here
