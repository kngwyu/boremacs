;;; init-rust.el --- Support for the Rust language -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package rustic
  :hook
  (rust-mode . (lambda ()
                 (setq-local fill-column 100)))
  :custom
  (rustic-format-trigger 'on-save)
  (rustic-lsp-client 'eglot))

(provide 'init-rust)
;;; init-rust.el ends here
