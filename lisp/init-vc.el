;;; init-vc.el --- Version control support -*- lexical-binding: t -*-
;;; Commentary:

;; Most version control packages are configured separately: see
;; init-git.el, for example.

;;; Code:


;; diff-hl
(use-package diff-hl
  :hook
  (dired-mode . diff-hl-dired-mode)
  (prog-mode . diff-hl-mode)
  (text-mode . diff-hl-mode))

;; GIT utilities
(use-package git-modes)

(use-package git-link
  :commands git-link git-link-commit)

(use-package git-messenger
  :bind
  (:map vc-prefix-map
        ("p" . git-messenger:popup-message)
        :map git-messenger-map
        ("m" . git-messenger:copy-message))
  :custom
  (git-messenger:show-detail t)
  (git-messenger:use-magit-popup t))

(use-package git-timemachine
  :straight (:type git :host github :repo "emacsmirror/git-timemachine")
  :bind ("C-c g t" . git-timemachine)
  :custom (git-timemachine-show-minibuffer-details t))

;; Magit
(use-package magit
  :after diff-hl
  :hook
  (magit-pre-refresh-hook . diff-hl-magit-pre-refresh)
  (magit-post-refresh-hook . diff-hl-magit-post-refresh)
  :init
  (setq magit-diff-refine-hunk t)
  :bind
  (("C-x g" . magit-status)
   ("C-x M-g" . magit-dispatch))
  :config
    ;; Workaround for https://github.com/magit/magit/issues/4676#issuecomment-1138139297
  (autoload 'transient--with-suspended-override "transient")

  (when (fboundp 'transient-append-suffix)
    ;; Add switch: --tags
    (transient-append-suffix 'magit-fetch
      "-p" '("-t" "Fetch all tags" ("-t" "--tags"))))

  ;; Exterminate Magit buffers
  ;; From https://github.com/seagle0128/.emacs.d/blob/master/lisp/init-vcs.el
  (defun boremacs/magit-kill-buffers (&rest _)
    "Restore window configuration and kill all Magit buffers."
    (interactive)
    (magit-restore-window-configuration)
    (let ((buffers (magit-mode-get-buffers)))
      (when (eq major-mode 'magit-status-mode)
        (mapc (lambda (buf)
                (with-current-buffer buf
                  (if (and magit-this-process
                           (eq (process-status magit-this-process) 'run))
                      (bury-buffer buf)
                    (kill-buffer buf)))) buffers))))
  (setq magit-bury-buffer-function #'boremacs/magit-kill-buffers)
  (fullframe magit-status magit-mode-quit-window))

;; magit-extras add useful [m] magit menu to project-switch-project
(use-package magit-extras
  :straight nil
  :after project)


;; magit-todo
(use-package magit-todos
  :after magit
  :defer t
  :defines magit-todos-nice
  :init
  (setq magit-todos-nice (if (boremacs/executable-find "nice") t nil))
  (let ((inhibit-message t))
    (magit-todos-mode 1))
  :config
  (with-eval-after-load 'magit-status
    (transient-append-suffix 'magit-status-jump '(0 0 -1)
      '("t " "Todos" magit-todos-jump-to-todos))))


;; Forge
(use-package forge
  :after magit
  :defer t
  :defines forge-topic-list-columns
  :if (boremacs/executable-find "cc")
  :custom-face
  (forge-topic-label
   ((t (:inherit variable-pitch :height 0.9 :width condensed :weight regular :underline nil))))
  :custom
  (forge-topic-list-columns
   '(("#" 5 forge-topic-list-sort-by-number (:right-align t) number nil)
     ("Title" 60 t nil title  nil)
     ("State" 6 t nil state nil)
     ("Updated" 10 t nil updated nil))))

(provide 'init-vc)
;;; init-vc.el ends here
