;;; init-misc.el --- Some random settings -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

;; Change the default config/data locations
(use-package no-littering)
(use-package diminish)
(use-package command-log-mode)
(use-package restart-emacs
             :bind ("C-x c" . restart-emacs))


;; Replace define-key by bind-key
(use-package bind-key :demand t)

;; define some miscellaneous bindings
(bind-keys
 ("C-h" . delete-backward-char)
 ("C-'" . set-mark-command)
 ("C-M-n" . forward-paragraph)
 ("C-M-p" . backward-paragraph))
(fset 'yes-or-no-p 'y-or-n-p)
(add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)
(defun sanityinc/set-mode-for-new-scripts ()
  "Invoke `normal-mode' if this file is a script and in `fundamental-mode'."
  (and
   (eq major-mode 'fundamental-mode)
   (>= (buffer-size) 2)
   (save-restriction
     (widen)
     (string= "#!" (buffer-substring (point-min) (+ 2 (point-min)))))
   (normal-mode)))
(add-hook 'after-save-hook 'sanityinc/set-mode-for-new-scripts)
(add-hook 'prog-mode-hook 'goto-address-prog-mode)
(use-package info-colors
  :hook (Info-selection-hook . info-colors-fontify-node))

;; Project
(use-package project
  :straight nil
  :bind ("C-x p a" . boremacs/project-remember-current-project)
  :config
  (defun boremacs/project-remember-current-project ()
    "Remeber the current project."
    (interactive)
    (project-remember-projects-under
     (file-name-directory buffer-file-name) nil)))

(provide 'init-misc)
;;; init-misc.el ends here
