;;; init-markdown.el --- Markdown support -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package markdown-mode
  :mode ("\\.md\\'" . markdown-mode)
  :hook (markdown-mode . display-fill-column-indicator-mode)
  :config (add-to-list 'whitespace-cleanup-mode-ignore-modes 'markdown-mode))

(use-package grip-mode
  :after markdown-mode
  :bind (:map markdown-mode-command-map ;; C-c C-c g to invoke this
              ("g" . grip-mode)))

(provide 'init-markdown)
;;; init-markdown.el ends here
