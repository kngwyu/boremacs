;;; init-sql.el --- SQL support -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package sql :straight nil)

(provide 'init-sql)
;;; init-sql.el ends here
