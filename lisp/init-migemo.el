;;; init-migemo.el --- migemo
;;; Commentary:
;;; Code:

(use-package migemo
  :commands migemo-init
  :if (boremacs/executable-find "cmigemo")
  :custom
  (migemo-command "cmigemo")
  (migemo-options '("-q" "--emacs"))
  ;; Set your installed path
  (migemo-dictionary (expand-file-name "/usr/share/migemo/utf-8/migemo-dict"))
  (migemo-user-dictionary nil)
  (migemo-regex-dictionary nil)
  (migemo-coding-system 'utf-8-unix)
  :init
  (migemo-init))

(provide 'init-migemo)
;;; init-migemo.el ends here
