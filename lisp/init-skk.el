;;; init-skk.el --- setup ddskk
;;; Commentary:
;;; Code:

(use-package skk
  :straight ddskk
  :defer t
  :bind (("C-x a f". 'auto-fill-mode)
         ("C-x j" . 'skk-mode))
  :custom
  (skk-show-candidates-nth-henkan-char 3)
  (skk-henkan-number-to-display-candidates 8)
  (skk-auto-insert-paren t)
  (skk-use-search-web t)
  :config
  (when (file-exists-p "/usr/share/skk/SKK-JISYO.L")
    (setq skk-large-jisyo "/usr/share/skk/SKK-JISYO.L")))

(provide 'init-skk)
;;; init-skk.el ends here
