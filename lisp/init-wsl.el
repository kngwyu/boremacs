;;; init-wsl.el --- WSL configuration  -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'cl-lib)

(when (and (eq system-type 'gnu/linux)
           (string-match "WSL" (shell-command-to-string "uname -r")))
;;; Opacity is still not great even in WSLg
  (setq boremacs/default-alpha 100)
  (setq boremacs/default-font-size 150)
  (setq
   browse-url-generic-program  "/mnt/c/Windows/System32/cmd.exe"
   browse-url-generic-args     '("/c" "start")
   browse-url-browser-function #'browse-url-generic)
  ;; Make copy work
  (defun boremacs/wsl-copy-selected-text (orig beg end &optional region)
    ;;; Work around for WSL clipboard issue
    (if (use-region-p)
        (let* ((raw-text (buffer-substring-no-properties beg end))
               (text (string-replace "'" "\\'" raw-text)))
          (shell-command
           (concat "echo '" text "' | iconv -t cp932 | clip.exe"))))
    (funcall orig beg end region))
  (advice-add 'copy-region-as-kill :around 'boremacs/wsl-copy-selected-text))

(provide 'init-wsl)
;;; init-wsl.el ends here
