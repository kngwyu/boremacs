;;; init-minibuffer-completion.el --- Config for minibuffer completion       -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

;; Set builtins
(setq enable-recursive-minibuffers t
      echo-keystrokes 0.02)

;; orderless
(use-package orderless
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion)))))

;; vertico
(defun boremacs/+vertico-truncate-candidates (args)
  (if-let ((arg (car args))
           (type (get-text-property 0 'multi-category arg))
           ((eq (car-safe type) 'file))
           (w (max 40 (- (window-width) 40)))
           (l (length arg))
           ((> l w)))
      (setcar args (concat "…" (truncate-string-to-width arg l (- l w))))) args)

(defun boremacs/+crm-indicator (args)
  (cons (format "[CRM%s] %s"
                (replace-regexp-in-string
                 "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                 crm-separator)
                (car args))
        (cdr args)))

(advice-add #'completing-read-multiple :filter-args #'boremacs/+crm-indicator)

;; Do not allow the cursor in the minibuffer prompt
(setq minibuffer-prompt-properties
      '(read-only t cursor-intangible t face minibuffer-prompt))
(add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

;; Emacs 28: Hide commands in M-x which do not work in the current mode.
;; Vertico commands are hidden in normal buffers.
(setq read-extended-command-predicate
      #'command-completion-default-include-p)

(use-package vertico
  :after orderless
  :straight (vertico :includes (vertico-directory vertico-mouse)
                     :files (:defaults "extensions/*.el"))
  :init
  (vertico-mode)
  (advice-add #'vertico--format-candidate :filter-args #'boremacs/+vertico-truncate-candidates)
  :bind
  (:map vertico-map
        ("M-n" . vertico-next-group)
        ("M-p" . vertico-previous-group)
        ("RET" . vertico-directory-enter)
        ("C-l" . vertico-directory-delete-word)))


;; embark
(use-package embark
  :after vertico xref
  :bind
  (("C-." . embark-act)
   ("M-." . embark-dwim)
   ("C-c h" . embark-bindings)
   :map vertico-map
   ;; Use the same key bind as org-export-dispatch
   ("C-c C-e" . embark-export))
  :init
  (setq prefix-help-command #'embark-prefix-help-command)
  :config
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))


;; consult
;; Custom consult functions
(defun boremacs/consult-line-file (&optional initial)
  "Search for a matching line forward with the INITIAL value."
  (interactive)
  (let* ((current-symbol (symbol-at-point))
         (initial (or initial
                      (if current-symbol
                          (symbol-name current-symbol) nil))))
    (consult-line initial t)))

(defun boremacs/consult-line-forward (&optional initial)
  "Search for a matching line forward with the INITIAL value."
  (interactive)
  (consult-line initial))

(defun boremacs/consult-line-backward (&optional initial)
  "Search for a matching line backward with the INITIAL value."
  (interactive)
  (advice-add 'consult--line-candidates :filter-return 'reverse)
  (vertico-reverse-mode +1)
  (unwind-protect (consult-line initial)
    (vertico-reverse-mode -1)
    (advice-remove 'consult--line-candidates 'reverse)))

(defun boremacs/consult--ripgrep-noignore-builder (input)
  "consult--ripgrep-builder with INPUT, but ignores .gitignore."
  (let ((consult-ripgrep-args
         (if (string-match-p "--no-ignore-vcs" consult-ripgrep-args)
             consult-ripgrep-args
           (concat consult-ripgrep-args " --no-ignore-vcs ."))))
    (if (featurep 'consult--ripgrep-builder)
        (consult--ripgrep-builder input)
      (consult--ripgrep-make-builder input))))

(defun boremacs/consult-ripgrep-noignore (&optional dir initial)
  "Do consult-ripgrep with DIR and INITIAL, but without ignoring."
  (interactive "P")
  (consult--grep "Ripgrep"
                 #'boremacs/consult--ripgrep-noignore-builder
                 (if dir dir t) initial))

(defun boremacs/consult-ripgrep-dir (dir)
  "Do consult-ripgrep in the given directory DIR."
  (interactive "dDo ripgrep in the given directory")
  (consult-ripgrep dir nil))

(defun boremacs/orderless-migemo-dispatcher (pattern _index _total)
  "An orderless dispatcher that convert the PATTERN to migemo pattern."
  (let ((migemo-pattern (migemo-get-pattern pattern)))
    (condition-case nil
        (progn (string-match-p migemo-pattern "") migemo-pattern)
      (invalid-regexp nil))))

(defvar boremacs/orderless-migemo-enabled nil)

(defun boremacs/enable-orderless-migemo ()
  "Enable orderless migemo in the current buffer."
  (interactive)
  (unless boremacs/orderless-migemo-enabled
    (setq-local orderless-style-dispatchers '(boremacs/orderless-migemo-dispatcher))
    (setq-local boremacs/orderless-migemo-enabled t)))

(defconst boremacs/orderless-migemo-keymap
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "M-m") #'boremacs/enable-orderless-migemo) map))

(defconst boremacs/consult--fd-command
  (cond ((executable-find "fdfind") "fdfind")
        ((executable-find "fd") "fd")
        (t nil)))

(defvar boremacs/consult--fd-args '("--color=never" "--full-path"))

(defun boremacs/consult--fd-builder (input)
  "Build fd command from INPUT."
  (pcase-let* ((`(,arg . ,opts) (consult--command-split input))
               (`(,re . ,hl) (funcall consult--regexp-compiler arg 'extended t)))
    (when re
      (cons (append
             (cons boremacs/consult--fd-command boremacs/consult--fd-args)
             (cons (consult--join-regexps re 'extended) opts))
       hl))))

(defun boremacs/consult--fd-builder-no-ignore (input)
  "Build fd command from INPUT with --no-ignore."
  (let ((boremacs/consult--fd-args '("--color=never" "--full-path" "--no-ignore")))
    (boremacs/consult--fd-builder input)))

(defun boremacs/consult-fd (&optional dir initial)
  "Do fd file search in DIR with INITIAL value."
  (interactive "P")
  (pcase-let* ((`(,prompt ,paths ,dir) (consult--directory-prompt "Fd" dir))
               (default-directory dir))
    (find-file (consult--find prompt #'boremacs/consult--fd-builder initial))))

(defun boremacs/consult-fd-no-ignore (&optional dir initial)
  "Do fd file search in DIR with INITIAL value."
  (interactive "P")
  (pcase-let* ((`(,prompt ,paths ,dir) (consult--directory-prompt "Fd" dir))
               (default-directory dir))
    (find-file (consult--find prompt #'boremacs/consult--fd-builder-no-ignore initial))))

(use-package consult
  :after vertico
  :commands consult--grep consult--find consult--directory-prompt
  :init
  (if (boremacs/executable-find "rg")
      (bind-keys
       ("M-s g" . consult-ripgrep)
       ("M-s n g" . boremacs/consult-ripgrep-noignore))
    (bind-key "M-s g" 'consult-grep))
  (if boremacs/consult--fd-command
      (bind-keys
       ("M-s d" . boremacs/consult-fd)
       ("M-s n d" . boremacs/consult-fd-no-ignore))
    (bind-key "M-s d" 'consult-find))
  :custom
  (consult-narrow-key "<")
  :bind
  (("M-s o" . boremacs/consult-line-file)
   ([remap switch-to-buffer] . consult-buffer)
   ([remap switch-to-buffer-other-window] . consult-buffer-other-window)
   ([remap switch-to-buffer-other-frame] . consult-buffer-other-frame)
   ([remap goto-line] . consult-goto-line)
   ([remap project-switch-to-buffer] . consult-project-buffer) ; C-x p b
   ([remap complex-command] . consult-complex-command)
   ("C-x M-r" . consult-complex-command)
   ("C-c C-o" . consult-file-externally)
   ("M-y" . consult-yank-pop)
   ("M-g f" . consult-flymake)
   :map isearch-mode-map
   ("C-n" . boremacs/consult-line-forward)
   ("C-p" . boremacs/consult-line-forward))
  :config
  (consult-customize
   boremacs/consult-line-forward :prompt "Forward (M-m for migemo): "
   boremacs/consult-line-backward :prompt "Backward (M-m for migemo): "
   boremacs/consult-line-file :prompt "Search (M-m for migemo): "
   boremacs/consult-line-forward :keymap boremacs/orderless-migemo-keymap
   boremacs/consult-line-backward :keymap boremacs/orderless-migemo-keymap
   boremacs/consult-line-file :keymap boremacs/orderless-migemo-keymap))

(use-package consult-dir
  :after consult
  :bind
  (("C-x C-d" . consult-dir)
   :map vertico-map
   ("C-x C-d" . consult-dir)
   ("C-x C-j" . consult-dir-jump-file)))


;; marginalia
(use-package marginalia
  :init
  (marginalia-mode))


;; affe
(use-package affe
  :after (consult orderless)
  :if (boremacs/executable-find "rg")
  :bind
  (("M-s f g" . affe-grep)
   ("M-s f d" . affe-find))
  :config
  (defun boremacs/affe-orderless-regexp-compiler (input _type _ignorecase)
    (setq input (orderless-pattern-compiler input))
    (cons input (lambda (str) (orderless--highlight input str))))
  (setq affe-regexp-compiler #'boremacs/affe-orderless-regexp-compiler))


;; recursion-indicator
(use-package recursion-indicator
  :init
  (recursion-indicator-mode))


;; Some grep configs
(use-package grep
  :straight nil
  :custom
  (grep-highlight-matches t)
  (grep-scroll-output t))

(use-package wgrep
  :custom (wgrep-auto-save-buffer t)
  :bind
  (:map grep-mode-map
        ("C-c C-e" . wgrep-change-to-wgrep-mode)
        ("e" . wgrep-change-to-wgrep-mode)))


;; embark-consult
(use-package embark-consult
  :after (embark consult)
  :bind
  (:map embark-file-map
        ("g" . boremacs/consult-ripgrep-dir))
  :demand t
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(provide 'init-minibuffer-completion)
;;; init-minibuffer-completion.el ends here
