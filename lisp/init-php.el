;;; init-php.el --- PHP editing -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package php-mode :mode "\\.php$")
(use-package neon-mode)

(provide 'init-php)
;;; init-php.el ends here
