;;; init-themes.el --- Defaults for themes -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(setq custom-safe-themes t)


;; modus-theme config
(use-package modus-themes
  :init
  (load-theme 'modus-vivendi t)
  :config
  ;; modeline configs
  (let ((line (face-attribute 'mode-line :underline)))
    (set-face-attribute 'mode-line          nil :overline   line)
    (set-face-attribute 'mode-line-inactive nil :overline   line)
    (set-face-attribute 'mode-line-inactive nil :underline  line)
    (set-face-attribute 'mode-line          nil :box        nil)
    (set-face-attribute 'mode-line-inactive nil :box        nil)))


;; Dimmer
(use-package dimmer
  :init
  (dimmer-mode)
  :custom
  (dimmer-fraction 0.15)
  :config
  (defun sanityinc/display-non-graphic-p ()
    (not (display-graphic-p)))
  (add-to-list 'dimmer-exclusion-predicates 'sanityinc/display-non-graphic-p))


;; Show a small scroll bar in modeline
(use-package mlscroll
  :init
  (if (display-graphic-p)
      (mlscroll-mode 1)
    (add-hook 'after-make-console-frame-hooks 'mlscroll-mode 90))
  :custom (mlscroll-in-color (face-attribute 'cursor :background)))

(setq-local
 nerd-fonts
 (seq-filter (lambda (f) (string-match "Symbols Nerd Font Mono" f)) (font-family-list)))

(use-package nerd-icons
  :if nerd-fonts)

(use-package doom-modeline
  :after mlscroll
  :init
  (doom-modeline-mode 1)
  :custom
  (doom-modeline-buffer-file-name-style 'truncate-with-project)
  (doom-modeline-project-detection 'project)
  (doom-modeline-icon )
  (doom-modeline-minor-modes (< 0 (length nerd-fonts)))
  :config
  (doom-modeline-def-modeline
    'main
    '(bar workspace-name window-number matches buffer-info remote-host buffer-position selection-info)
    '(misc-info github debug repl lsp minor-modes input-method indent-info major-mode process vcs)))

(use-package minions
  :config (minions-mode 1)
  :custom (minions-mode-line-lighter "⚙"))

;; Breakline
(use-package page-break-lines
  :init (global-page-break-lines-mode 1)
  :diminish page-break-lines-mode)


;; Dashboard
(use-package dashboard
  :after page-break-lines
  :diminish dashboard-mode
  :hook (dashboard-mode . (lambda ()
                            (setq-local frame-title-format nil)
                            (page-break-lines-mode 1)))
  :init
  (dashboard-setup-startup-hook)
  :custom-face (dashboard-heading ((t (:inherit (font-lock-string-face bold)))))
  :custom
  (dashboard-projects-backend 'project-el)
  (dashboard-projects-switch-function 'project-switch-project)
  (dashboard-items '((projects . 8)
                     (recents  . 5)
                     (bookmarks . 5)))
  (dashboard-set-heading-icons t)
  (dashboard-set-file-icons t)
  (dashboard-set-navigator t)
  (dashboard-startup-banner 'logo)
  (dashboard-banner-logo-title
   "The dead can survive as part of the lives of those that still live. ― Hiroshima Notes")
  (dashboard-center-content t)
  (dashboard-set-footer nil))


;; scratch
(setq initial-scratch-message nil
      initial-major-mode 'text-mode)

;; Font settings
;; Use HackGen for coding
(defvar boremacs/default-font-size 120
  "Default font size")
(when-let
    ((hg-font (car
               (seq-filter
                (lambda (font) (member font (font-family-list)))
                '("HackGen35 Console NFJ" "HackGen35Nerd" "HackGen35 Console" "HackGen Console"))))
     (font-size (if (> (x-display-pixel-width) 3000) (+ 30 boremacs/default-font-size) boremacs/default-font-size)))
  (set-face-attribute 'default nil :family hg-font :height font-size)
  (set-fontset-font t 'japanese-jisx0208 (font-spec :family hg-font :height font-size)))

;; Use Source Code Pro for Japanese
(when (member "Source Code Pro" (font-family-list))
  (set-fontset-font t 'japanese-jisx0208 (font-spec :family "Source Code Pro" :height 120)))
(provide 'init-themes)
;;; init-themes.el ends here
