;;; init-code-completion.el --- Config for code completion       -*- lexical-binding: t; -*-
;;; Commentary: Corfu configuration
;;; Code:



;; corfu for code completion
(use-package corfu
  :after orderless
  :straight (corfu :includes corfu-popupinfo
                   :files (:defaults "extensions/corfu-popupinfo.el"))
  :init (global-corfu-mode)
  :custom
  (corfu-cycle t)
  (corfu-auto t)
  (corfu-quit-no-match 'separator)
  (corfu-scroll-margin 10)
  ;; Orderless field separator
  (corfu-separator ?\s))


;; Change dabbrev binds because I want to use M-/ for redo
;; M-= is count-words-region by default
(use-package dabbrev
  :straight nil
  :bind (("C-=" . dabbrev-expand)
         ("M-=" . dabbrev-completion)))


;; Cape backend for corfu
(use-package cape
  ;; Bind dedicated completion commands
  ;; Alternative prefix keys: C-c p, M-p, M-+, ...
  :bind (("C-c p p" . completion-at-point) ;; capf
         ("C-c p t" . complete-tag)        ;; etags
         ("C-c p d" . cape-dabbrev)        ;; or dabbrev-completion
         ("C-c p h" . cape-history)
         ("C-c p f" . cape-file)
         ("C-c p k" . cape-keyword)
         ("C-c p s" . cape-symbol)
         ("C-c p a" . cape-abbrev)
         ("C-c p i" . cape-ispell)
         ("C-c p l" . cape-line)
         ("C-c p w" . cape-dict)
         ("C-c p \\" . cape-tex)
         ("C-c p _" . cape-tex)
         ("C-c p ^" . cape-tex)
         ("C-c p &" . cape-sgml)
         ("C-c p r" . cape-rfc1345))
  :init
  ;; Add `completion-at-point-functions', used by `completion-at-point'.
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-keyword)
  (add-to-list 'completion-at-point-functions #'cape-tex)
  (add-to-list 'completion-at-point-functions #'cape-history))


;; Show icons for corfu
(use-package kind-icon
  :after corfu
  :custom
  (kind-icon-default-face 'corfu-default) ; to compute blended backgrounds correctly
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))


;; Some related global settings
(setq completion-cycle-threshold 4
      completion-category-defaults nil
      completion-category-overrides nil
      tab-always-indent 'complete)

(provide 'init-code-completion)
;;; init-code-completion.el ends here
