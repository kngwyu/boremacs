;;; init-sessions.el --- Save and restore editor sessions between restarts -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

;; Restore histories and registers after saving
(use-package savehist
  :straight nil
  :init (savehist-mode 1)
  :custom
  (savehist-additional-variables
   '(kill-ring
     command-history
     read-expression-history
     read-char-history
     face-name-history
     bookmark-history
     file-name-history
     mark-ring
     global-mark-ring
     search-ring))
  (savehist-save-minibuffer-history t)
  (savehist-autosave-interval nil)
  :config
  (setq history-length 1000
        history-delete-duplicates t
        kill-ring-max 50))

;; Save position in buffer
(use-package saveplace
  :straight nil
  :init (save-place-mode 1))


;; Save sessions using builtin desktop.el
(use-package desktop
  :straight nil
  :init (desktop-save-mode t)
  :custom
  (desktop-restore-frames nil)
  (desktop-restore-eager 2)
  (desktop-auto-save-timeout 100)
  (desktop-lazy-idle-delay 10)
  (desktop-lazy-verbose nil)
  (desktop-globals-to-save
   '((comint-input-ring        . 50)
     (compile-history          . 30)
     (dired-regexp-history     . 20)
     (extended-command-history . 30)
     (face-name-history        . 10)
     (file-name-history        . 100)
     (grep-find-history        . 30)
     (grep-history             . 30)
     (magit-revision-history   . 50)
     (minibuffer-history       . 50)
     (query-replace-history    . 10)
     (read-expression-history  . 10)
     (search-ring              . 20)
     tags-file-name
     tags-table-list))
  :config
  (defun sanityinc/time-subtract-millis (b a)
    (* 1000.0 (float-time (time-subtract b a))))

  (defun sanityinc/desktop-time-restore (orig &rest args)
    (let ((start-time (current-time)))
      (prog1
          (apply orig args)
        (message "Desktop restored in %.2fms"
                 (sanityinc/time-subtract-millis (current-time)
                                                 start-time)))))
  (advice-add 'desktop-read :around 'sanityinc/desktop-time-restore)

  (defun sanityinc/desktop-time-buffer-create (orig ver filename &rest args)
    (let ((start-time (current-time)))
      (prog1
          (apply orig ver filename args)
        (message "Desktop: %.2fms to restore %s"
                 (sanityinc/time-subtract-millis (current-time)
                                                 start-time)
                 (when filename
                   (abbreviate-file-name filename))))))

  (advice-add 'desktop-create-buffer :around 'sanityinc/desktop-time-buffer-create))

(provide 'init-sessions)
;;; init-sessions.el ends here
