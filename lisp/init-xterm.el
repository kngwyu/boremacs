;;; init-xterm.el --- Integrate with terminals such as xterm -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'init-frame-hooks)

(bind-keys
 ("<mouse-4>" . (lambda () (interactive) (scroll-down 1)))
 ("<mouse-5>" . (lambda () (interactive) (scroll-up 1))))

(add-hook 'after-make-console-frame-hooks 'mouse-wheel-mode)

(provide 'init-xterm)
;;; init-xterm.el ends here
