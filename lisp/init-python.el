;;; init-python.el --- Python editing -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(setq auto-mode-alist
      (append '(("SConstruct\\'" . python-mode)
                ("SConscript\\'" . python-mode))
              auto-mode-alist))

(use-package python
  :straight (python :type built-in)
  :hook
  (;; Don't use eglot by default for now
   ;; (python-mode . boremacs/eglot-lazy-ensure)
   (python-mode . (lambda ()
                    (setq-local fill-column 88))))
  :custom
  (python-shell-interpreter "python3")
  (python-indent-def-block-scale 1)
  :config
  (setf (alist-get 'isort apheleia-formatters)
        '("isort" "--stdout" "-"))
  (setf (alist-get 'python-mode apheleia-mode-alist)
        '(isort ruff)))


(use-package flymake-ruff
  :after python
  :when (boremacs/executable-find "uvx")
  :hook (python-mode . flymake-ruff-load)
  :custom (flymake-ruff-program "uvx ruff") )

(use-package cython-mode :mode "\\.pyx\\'")
(use-package pip-requirements :mode "\\.in\\'")

(provide 'init-python)
;;; init-python.el ends here
