;;; init-template.el --- Insert description here -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

;; Configure Tempel
(use-package tempel
  :custom
  (tempel-trigger-prefix "<")
  (tempel-path (expand-file-name "templates.eld" user-emacs-directory))
  :bind (("C-c t c" . tempel-complete) ;; Alternative tempel-expand
         ("C-c t i" . tempel-insert)
         :map tempel-map
         ("C-M-n" . tempel-next)
         ("C-M-p" . tempel-previous)
         ("M-RET" . tempel-done))
  :config
  ;; Setup completion at point
  (defun tempel-setup-capf ()
    ;; Add the Tempel Capf to `completion-at-point-functions'.
    ;; `tempel-expand' only triggers on exact matches. Alternatively use
    ;; `tempel-complete' if you want to see all matches, but then you
    ;; should also configure `tempel-trigger-prefix', such that Tempel
    ;; does not trigger too often when you don't expect it. NOTE: We add
    ;; `tempel-expand' *before* the main programming mode Capf, such
    ;; that it will be tried first.
    (setq-local completion-at-point-functions
                (cons #'tempel-expand
                      completion-at-point-functions)))
  (add-hook 'prog-mode-hook 'tempel-setup-capf)
  (add-hook 'text-mode-hook 'tempel-setup-capf))

(provide 'init-template)
;;; init-template.el ends here
