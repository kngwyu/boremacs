;;; init-web.el --- HTML, css, and their families -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package web-mode
  :straight nil
  :mode ("\\.html?\\'"
         "\\.tsx\\'"
         "\\.jsx\\'"))

(provide 'init-web)
;;; init-web.el ends here
