;;; init-editing-utils.el --- Day-to-day editing helpers -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package unfill)

(use-package elec-pair
  :straight nil
  :init
  (electric-pair-mode 1)
  (electric-indent-mode 1))

;; TODO: consult-unicode
;; TODO: Use emojify or some emoji packages?
;; TODO: iedit/multiple-cursor like utility

;;; ediff
(use-package ediff
  :straight nil
  :custom
  (ediff-diff-options "-w")
  (ediff-split-window-function 'split-window-horizontally)
  (ediff-window-setup-function 'ediff-setup-windows-plain))

;;; Some basic preferences

(setq-default
 blink-cursor-interval 0.4
 buffers-menu-max-size 30
 case-fold-search t
 column-number-mode t
 indent-tabs-mode nil
 create-lockfiles nil
 auto-save-default nil
 make-backup-files nil
 mouse-yank-at-point t
 save-interprogram-paste-before-kill t
 scroll-preserve-screen-position 'always
 set-mark-command-repeat-pop t
 tooltip-delay 1.5
 truncate-lines nil
 truncate-partial-width-windows nil)

(add-hook 'after-init-hook 'delete-selection-mode)


(use-package autorevert
  ;; revert buffers when their files/state have changed
  :hook ((focus-in after-save) . boremacs/auto-revert-buffers-h)
  :hook ((window-buffer-change-functions
          window-selection-change-functions) . boremacs/auto-revert-buffer-h)
  :config
  ;; auto revert settings is borrowed from doom
  (defun boremacs/auto-revert-buffer-h ()
    "Auto revert current buffer, if necessary."
    (unless (or auto-revert-mode (active-minibuffer-window))
      (let ((auto-revert-mode t))
        (auto-revert-handler))))

  (defun boremacs/auto-revert-buffers-h ()
    "Auto revert stale buffers in visible windows, if necessary."
    (dolist (buf (delete-dups (mapcar #'window-buffer (window-list))))
      (with-current-buffer buf
        (boremacs/auto-revert-buffer-h))))
  :custom
  (auto-revert-verbose t)
  (auto-revert-use-notify nil)
  (auto-revert-stop-on-user-input nil)
  (revert-without-query (list ".")))

(add-hook 'after-init-hook 'transient-mark-mode)


;; Huge files
(use-package so-long
  :straight nil
  :init
  (so-long-enable))

(use-package vlf
  :commands vlf
  :config
  (defun ffap-vlf ()
    "Find file at point with VLF."
    (interactive)
    (let ((file (ffap-file-at-point)))
      (unless (file-exists-p file)
        (error "File does not exist: %s" file))
      (vlf file))))


;;; A simple visible bell which works in all terminal types
(use-package mode-line-bell
  :init
  (mode-line-bell-mode 1))


(use-package beacon
  :init
  (beacon-mode 1)
  :custom
  (beacon-lighter "")
  (beacon-blink-when-point-moves-horizontally 100)
  (beacon-blink-when-point-moves-vertically 20)
  (beacon-color 0.4))


;;; Newline behaviour

(bind-key "RET" 'newline-and-indent)



(use-package subword
  :straight nil
  :config
  (diminish 'subword-mode))



(use-package display-line-numbers
  :straight nil
  :config
  (setq display-line-numbers-width 3)
  :hook
  (prog-mode . display-line-numbers-mode))



(use-package display-fill-column
  :straight nil
  :config
  (setq indicate-buffer-boundaries 'left
        display-fill-column-indicator-character ?\u254e)
  :hook
  (prog-mode . display-fill-column-indicator-mode)             )



(use-package rainbow-delimiters
  :hook
  (prog-mode . rainbow-delimiters-mode))


(use-package symbol-overlay
  :hook
  ((prog-mode html-mode yaml-mode conf-mode toml-mode) . symbol-overlay-mode)
  :diminish symbol-overlay-mode
  :bind
  (:map symbol-overlay-mode-map
        ("M-i" . symbol-overlay-put)
        ("M-I" . symbol-overlay-remove-all)
        ("M-n" . symbol-overlay-jump-next)
        ("M-p" . symbol-overlay-jump-prev)))


;;; indent guide
(use-package highlight-indent-guides
  :hook (prog-mode . highlight-indent-guides-mode)
  :custom (highlight-indent-guides-method 'character))

;;; Zap *up* to char is a handy pair for zap-to-char
(bind-key "M-z" 'zap-up-to-char)



;; Don't disable narrowing commands
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-defun 'disabled nil)
;; Don't disable case-change functions
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; Show matching parens
(add-hook 'after-init-hook 'show-paren-mode)


;;; Handy key bindings

(defun kill-back-to-indentation ()
  "Kill from point first non-whitespace character on the line."
  (interactive)
  (let ((prev-pos (point)))
    (back-to-indentation)
    (kill-region (point) prev-pos)))

(global-set-key (kbd "C-M-<backspace>") 'kill-back-to-indentation)

;;; Fix backward-up-list to understand quotes, see http://bit.ly/h7mdIL

(defun sanityinc/backward-up-sexp (arg)
  "Jump up to the start of the ARG'th enclosing sexp."
  (interactive "p")
  (let ((ppss (syntax-ppss)))
    (cond ((elt ppss 3)
           (goto-char (elt ppss 8))
           (sanityinc/backward-up-sexp (1- arg)))
          ((backward-up-list arg)))))

(bind-key [remap backward-up-list] 'sanityinc/backward-up-sexp) ; C-M-u, C-M-up



;;; Cut/copy the current line if no region is active
(use-package whole-line-or-region
  :init (whole-line-or-region-global-mode 1)
  :diminish whole-line-or-region-local-mode)



(defun sanityinc/open-line-with-reindent (n)
  "A version of `open-line' which reindents the start and end positions.
If there is a fill prefix and/or a `left-margin', insert them
on the new line if the line would have been blank.
With arg N, insert N newlines."
  (interactive "*p")
  (let* ((do-fill-prefix (and fill-prefix (bolp)))
         (do-left-margin (and (bolp) (> (current-left-margin) 0)))
         (loc (point-marker))
         ;; Don't expand an abbrev before point.
         (abbrev-mode nil))
    (delete-horizontal-space t)
    (newline n)
    (indent-according-to-mode)
    (when (eolp)
      (delete-horizontal-space t))
    (goto-char loc)
    (while (> n 0)
      (cond ((bolp)
             (if do-left-margin (indent-to (current-left-margin)))
             (if do-fill-prefix (insert-and-inherit fill-prefix))))
      (forward-line 1)
      (setq n (1- n)))
    (goto-char loc)
    (end-of-line)
    (indent-according-to-mode)))

(bind-key "C-o" 'sanityinc/open-line-with-reindent)



;; M-^ is inconvenient, so also bind M-j
(bind-key "M-j" 'join-line)



(use-package highlight-escape-sequences
  :init (hes-mode 1))


(use-package which-key
  :init (which-key-mode 1)
  :custom (which-key-idle-delay 1.5)
  :diminish which-key-mode)

(defun sanityinc/disable-features-during-macro-call (orig &rest args)
  "When running a macro, disable features that might be expensive.
ORIG is the advised function, which is called with its ARGS."
  (let (post-command-hook
        font-lock-mode
        (tab-always-indent (or (eq 'complete tab-always-indent) tab-always-indent)))
    (apply orig args)))

(advice-add 'kmacro-call-macro :around 'sanityinc/disable-features-during-macro-call)

(use-package editorconfig
  :init
  (editorconfig-mode 1))


;; Code minimap
(use-package minimap
  :custom
  (minimap-major-modes '(prog-mode))
  (minimap-window-location 'right)
  (minimap-update-delay 0.2)
  (minimap-minimum-width 20)
  :config
  (custom-set-faces
   '(minimap-active-region-background
     ((((background dark)) (:background "#555555555555"))
      (t (:background "#C847D8FEFFFF"))) :group 'minimap))
  :bind ("C-c m" . minimap-mode))


;; undo-tree

(use-package undo-tree
  :init (global-undo-tree-mode)
  :bind ("M-/" . undo-tree-redo)
  :custom
  (undo-tree-auto-save-history t)
  (undo-tree-enable-undo-in-region nil)
  (undo-tree-visualizer-diff nil)
  (undo-limit 800000 )
  (undo-strong-limit 12000000)
  (undo-outer-limit 128000000)
  (undo-tree-history-directory-alist
        `(("." . ,(expand-file-name "undo-tree-save-dir" user-emacs-directory))))
  :config
  ;; Use zstd for compressing
  (when (executable-find "zstd")
    (advice-add #'undo-tree-make-history-save-file-name
                :filter-return
                (lambda (file) (concat file ".zst"))))
  ;; This is noisy...
  (setq jka-compr-verbose nil)
  ;; Strip text properties from undo-tree data to stave off bloat. File size
  ;; isn't the concern here; undo cache files bloat easily, which can cause
  ;; freezing, crashes, GC-induced stuttering or delays when opening files.
  (defun boremacs/+undo--strip-text-properties (&rest _)
    (dolist (item buffer-undo-list)
      (and (consp item)
           (stringp (car item))
           (setcar item (substring-no-properties (car item))))))
  (advice-add #'boremacs/+undo--strip-text-properties
              :before #'undo-list-transfer-to-tree)

  ;; Supress buffer messages
  (defun boremacs/+undo--shut-up-messages (fn &rest args)
    (let ((inhibit-message t))
      (apply fn args)))
  (advice-add #'undo-tree-save-history :around #'boremacs/+undo--shut-up-messages))

(use-package sudo-edit
  :commands sudo-edit)

(use-package smartparens
  :commands smartparens-mode)

(use-package apheleia
  :custom
  (aphaleia-global-mode +1))

(provide 'init-editing-utils)
;;; init-editing-utils.el ends here
