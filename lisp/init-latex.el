;;; init-latex.el --- LATEX files -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(defun latex-indent-item-fn ()
  "Indent LaTeX \"itemize\",\"enumerate\", and \"description\" environments.
\"\\item\" is indented `LaTeX-indent-level' spaces relative to the the beginning
of the environment.
See `LaTeX-indent-level-item-continuation' for the indentation strategy this
function uses."
  (save-match-data
    (let* ((re-beg "\\\\begin{")
           (re-end "\\\\end{")
           (re-env "\\(?:itemize\\|\\enumerate\\|description\\)")
           (indent (save-excursion
                     (when (looking-at (concat re-beg re-env "}"))
                       (end-of-line))
                     (LaTeX-find-matching-begin)
                     (+ LaTeX-item-indent (current-column))))
           (contin 6))
      (cond ((looking-at (concat re-beg re-env "}"))
             (or (save-excursion
                   (beginning-of-line)
                   (ignore-errors
                     (LaTeX-find-matching-begin)
                     (+ (current-column)
                        LaTeX-item-indent
                        LaTeX-indent-level
                        (if (looking-at (concat re-beg re-env "}"))
                            contin
                          0))))
                 indent))
            ((looking-at (concat re-end re-env "}"))
             (save-excursion
               (beginning-of-line)
               (ignore-errors
                 (LaTeX-find-matching-begin)
                 (current-column))))
            ((looking-at "\\\\item")
             (+ LaTeX-indent-level indent))
            ((+ contin LaTeX-indent-level indent))))))

(defun latex-fold-last-macro-a (&rest _)
  "Advice to auto-fold LaTeX macros after functions that
typically insert macros."
  ;; A simpler approach would be to just fold the whole line, but if point was
  ;; inside a macro that would kick it out. So instead we fold the last macro
  ;; before point, hoping its the one newly inserted.
  (TeX-fold-region (save-excursion
                     (search-backward "\\" (line-beginning-position) t)
                     (point))
                   (1+ (point))))

(use-package tex
  :straight auctex
  :mode ("\\.tex\\'" . LaTeX-mode)
  :hook
  ((TeX-mode . (lambda () (setq ispell-parser 'tex)))
   (TeX-mode . (lambda () (setq fill-nobreak-predicate (cons #'texmathp fill-nobreak-predicate))))
   (TeX-mode . visual-line-mode)
   (LaTeX-mode . smartparens-mode)
   (TeX-update-style-hook . rainbow-delimiters-mode))
  :init
  ;; texlab language server
  (when (boremacs/executable-find "texlab")
    (with-eval-after-load 'eglot
      (add-hook 'TeX-mode-hook #'boremacs/eglot-lazy-ensure)
      (add-to-list
       'eglot-server-programs
       '(tex-mode "texlab"))))
  :custom
  (TeX-parse-self t)
  (TeX-auto-save t)
  (TeX-auto-local ".auctex-auto")
  (TeX-style-local ".auctex-style")
  (TeX-source-correlate-mode t)
  (TeX-source-correlate-method 'synctex)
  (TeX-source-correlate-start-server nil)
  (TeX-electric-sub-and-superscript t)
  (TeX-save-query nil)
  (LaTeX-section-hook
   '(LaTeX-section-heading
     LaTeX-section-title
     LaTeX-section-toc
     LaTeX-section-section
     LaTeX-section-label))
  (LaTeX-fill-break-at-separators nil)
  (LaTeX-item-indent 0)
  :config
  (setq font-latex-match-reference-keywords
        '(;; BibLaTeX.
          ("printbibliography" "[{")
          ("addbibresource" "[{")
          ;; Standard commands.
          ("cite" "[{")
          ("citep" "[{")
          ("citet" "[{")
          ("Cite" "[{")
          ("parencite" "[{")
          ("Parencite" "[{")
          ("footcite" "[{")
          ("footcitetext" "[{")
          ;; Style-specific commands.
          ("textcite" "[{")
          ("Textcite" "[{")
          ("smartcite" "[{")
          ("Smartcite" "[{")
          ("cite*" "[{")
          ("parencite*" "[{")
          ("supercite" "[{")
          ;; Qualified citation lists.
          ("cites" "[{")
          ("Cites" "[{")
          ("parencites" "[{")
          ("Parencites" "[{")
          ("footcites" "[{")
          ("footcitetexts" "[{")
          ("smartcites" "[{")
          ("Smartcites" "[{")
          ("textcites" "[{")
          ("Textcites" "[{")
          ("supercites" "[{")
          ;; Style-independent commands.
          ("autocite" "[{")
          ("Autocite" "[{")
          ("autocite*" "[{")
          ("Autocite*" "[{")
          ("autocites" "[{")
          ("Autocites" "[{")
          ;; Text commands.
          ("citeauthor" "[{")
          ("Citeauthor" "[{")
          ("citetitle" "[{")
          ("citetitle*" "[{")
          ("citeyear" "[{")
          ("citedate" "[{")
          ("citeurl" "[{")
          ;; Special commands.
          ("fullcite" "[{")
          ;; Cleveref.
          ("cref" "{")
          ("Cref" "{")
          ("cpageref" "{")
          ("Cpageref" "{")
          ("cpagerefrange" "{")
          ("Cpagerefrange" "{")
          ("crefrange" "{")
          ("Crefrange" "{")
          ("labelcref" "{")))

  (setq font-latex-match-textual-keywords
        '(;; BibLaTeX brackets.
          ("parentext" "{")
          ("brackettext" "{")
          ("hybridblockquote" "[{")
          ;; Auxiliary commands.
          ("textelp" "{")
          ("textelp*" "{")
          ("textins" "{")
          ("textins*" "{")
          ;; Subcaption.
          ("subcaption" "[{")))

  (setq font-latex-match-variable-keywords
        '(;; Amsmath.
          ("numberwithin" "{")
          ;; Enumitem.
          ("setlist" "[{")
          ("setlist*" "[{")
          ("newlist" "{")
          ("renewlist" "{")
          ("setlistdepth" "{")
          ("restartlist" "{")
          ("crefname" "{")))
  ;; Viewer
  (when (executable-find "okular")
    (add-to-list 'TeX-view-program-list
                 '("Okular" ("okular --noraise --unique file:%o" (mode-io-correlate "#src:%n%a"))))
    (add-to-list 'TeX-view-program-selection '(output-pdf "Okular")))
  (when (executable-find "evince")
    (add-to-list 'TeX-view-program-selection '(output-pdf "Evince")))
  ;; Do not prompt for a master file.
  (setq-default TeX-master t)
  ;; Set-up chktex.
  (setcar (cdr (assoc "Check" TeX-command-list)) "chktex -v6 -H %s")
  (with-eval-after-load 'latex
    (dolist (env '("itemize" "enumerate" "description"))
      (add-to-list 'LaTeX-indent-environment-list `(,env +latex-indent-item-fn))))

  (defun boremacs/+latex--re-indent-itemize-and-enumerate (fn &rest args)
    (let ((LaTeX-indent-environment-list
           (append LaTeX-indent-environment-list
                   '(("itemize"   +latex-indent-item-fn)
                      ("enumerate" +latex-indent-item-fn)))))
      (apply fn args)))
  (advice-add #'LaTeX-fill-region-as-para-do :around #'boremacs/+latex--re-indent-itemize-and-enumerate)

  (defun boremacs/+latex--dont-indent-itemize-and-enumerate (fn &rest args)
    (let ((LaTeX-indent-environment-list LaTeX-indent-environment-list))
      (assq-delete-all "itemize" LaTeX-indent-environment-list)
      (assq-delete-all "enumerate" LaTeX-indent-environment-list)
      (apply fn args)))
  (advice-add #'LaTeX-fill-region-as-paragraph :around #'boremacs/+latex--dont-indent-itemize-and-enumerate))


;; Other LaTex packages
(use-package cdlatex
  :after tex
  :hook
  ((LaTeX-mode . cdlatex-mode)
   (org-mode . org-cdlatex-mode))
  :custom
  (cdlatex-use-dollar-to-ensure-math nil))

(use-package adaptive-wrap)

(use-package math-preview
  :when (boremacs/executable-find "math-preview")
  :custom (math-preview-command "math-preview"))

;; Citation
(use-package reftex
  :straight nil
  :hook
  (LaTeX-mode . turn-on-reftex)
  :config
  (require 'reftex-cite)
  (require 'reftex-parse)
  :custom
  (reftex-default-bibliography org-cite-global-bibliography))

(use-package citar
  :after oc reftex
  :bind
  (("C-c b" . citar-insert-citation)
   :map minibuffer-local-map
   ("M-b" . citar-insert-preset))
  :custom
  (citar-symbol-separator "  ")
  (citar-bibliography org-cite-global-bibliography)
  (citar-latex-default-cite-command "citep")
  :config
  (defvar citar-indicator-files-icons
    (citar-indicator-create
       :symbol (nerd-icons-faicon
                "nf-fa-file_o"
                :face 'nerd-icons-green
                :v-adjust -0.1)
       :function #'citar-has-files
       :padding "  " ; need this because the default padding is too low for these icons
       :tag "has:files"))
  (defvar citar-indicator-links-icons
      (citar-indicator-create
       :symbol (nerd-icons-faicon
                "nf-fa-link"
                :face 'nerd-icons-orange
                :v-adjust 0.01)
       :function #'citar-has-links
       :padding "  "
       :tag "has:links"))
  (defvar citar-indicator-notes-icons
    (citar-indicator-create
       :symbol (nerd-icons-codicon
                "nf-cod-note"
                :face 'nerd-icons-blue
                :v-adjust -0.3)
       :function #'citar-has-notes
       :padding "    "
       :tag "has:notes"))
  (defvar citar-indicator-cited-icons
    (citar-indicator-create
       :symbol (nerd-icons-faicon
                "nf-fa-circle_o"
                :face 'nerd-icon-green)
       :function #'citar-is-cited
       :padding "  "
       :tag "is:cited"))
  (setq citar-indicators
        (list citar-indicator-files-icons
              citar-indicator-links-icons
              citar-indicator-notes-icons
              citar-indicator-cited-icons))
  (defun boremacs/+copy-local-bib-to-main (fn &rest args)
    (apply fn args)
    (let* ((ext (file-name-extension (car citar-bibliography)))
           (dir (file-name-directory buffer-file-name))
           (localfile (format "%slocal-bib.%s" dir ext))
           (mainfile (car (reftex-locate-bibliography-files dir))))
      (when (and mainfile (file-exists-p localfile))
        (copy-file localfile mainfile t)
        (message "Copied %s to %s" localfile mainfile))))
  (advice-add #'citar-export-local-bib-file :around #'boremacs/+copy-local-bib-to-main))

(use-package citar-embark
  :after citar embark
  :config (citar-embark-mode))

(use-package citar-capf
  :after citar embark
  :straight nil
  :init
  (add-to-list 'completion-at-point-functions #'citar-capf))

(use-package citar-latex
  :after citar reftex
  :straight nil
  :config
  ;; Use the latest reftex function since the older version has a regex bug
  (defun boremacs/reftex-all-used-citation-keys ()
    "Return a list of all citation keys used in document."
    (reftex-access-scan-info)
    ;; FIXME: multicites macros provided by biblatex
    ;; are not covered in this function.
    (let ((files (reftex-all-document-files))
          (re (concat "\\\\"
                      "\\(?:"
                      ;; biblatex volcite macros take these args:
                      ;; \volcite[prenote]{volume}[pages]{key}
                      ;; so cater for the first 3 args:
                      (regexp-opt '("volcite"  "Volcite"
                                    "pvolcite" "Pvolcite"
                                    "fvolcite" "ftvolcite"
                                    "svolcite" "Svolcite"
                                    "tvolcite" "Tvolcite"
                                    "avolcite" "Avolcite"))
                      "\\(?:\\[[^]]*\\]\\)?"
                      "{[^}]*}"
                      "\\(?:\\[[^]]*\\]\\)?"
                      "\\|"
                      ;; Other cite macros usually go like:
                      ;; \cite[prenote][postnote]{key}
                      ;; so cater for the optional args:
                      "\\(?:bibentry\\|[a-zA-Z]*[Cc]ite[a-zA-Z*]*\\)"
                      "\\(?:\\[[^]]*\\]\\)\\{0,2\\}"
                      "\\)"
                      ;; Now match the key:
                      "{\\([^}]+\\)}"))
          file keys kk k)
      (save-current-buffer
        (while (setq file (pop files))
          (set-buffer (reftex-get-file-buffer-force file 'mark))
          (save-excursion
            (save-restriction
              (widen)
              (goto-char (point-min))
              (while (re-search-forward re nil t)
                ;; Make sure we're not inside a comment:
                (unless (save-match-data
                          (nth 4 (syntax-ppss)))
                  (setq kk (match-string-no-properties 1))
                  (while (string-match "%.*\n?" kk)
                    (setq kk (replace-match "" t t kk)))
                  (setq kk (split-string kk "[, \t\r\n]+"))
                  (while (setq k (pop kk))
                    (or (member k keys)
                        (setq keys (cons k keys))))))))))
      (reftex-kill-temporary-buffers)
      keys))
  (defalias 'citar-latex-list-keys #'boremacs/reftex-all-used-citation-keys))

(provide 'init-latex)
;;; init-latex.el ends here
