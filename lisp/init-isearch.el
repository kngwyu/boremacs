;;; init-isearch.el --- isearch customisations -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package isearch
  :straight nil
  :bind
  (:map isearch-mode-map
        ([remap isearch-delete-char] . isearch-del-char)
        ("C-M-w" . boremacs/isearch-yank-symbol))
  :config
  ;; Search back/forth for the symbol at point
  ;; See http://www.emacswiki.org/emacs/SearchAtPoint
  (defun boremacs/isearch-yank-symbol ()
    "*Put symbol at current point into search string."
    (interactive)
    (let ((sym (thing-at-point 'symbol)))
      (if sym
          (progn
            (setq isearch-regexp t
                  isearch-string (concat "\\_<" (regexp-quote sym) "\\_>")
                  isearch-message (mapconcat 'isearch-text-char-description isearch-string "")
                  isearch-yank-flag t))
        (ding)))
    (isearch-search-and-update)))

(use-package anzu
  :init (global-anzu-mode)
  :bind
  (([remap query-replace-regexp] . anzu-query-replace-regexp)
   ([remap query-replace] . anzu-query-replace)))


(defun sanityinc/isearch-exit-other-end ()
  "Exit isearch, but at the other end of the search string.
This is useful when followed by an immediate kill."
  (interactive)
  (isearch-exit)
  (goto-char isearch-other-end))

(define-key isearch-mode-map [(control return)] 'sanityinc/isearch-exit-other-end)

(provide 'init-isearch)
;;; init-isearch.el ends here
