;;; init-bash.el --- Support for writing bash scripts -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package shfmt
  :if (boremacs/executable-find "shfmt")
  :hook (sh-mode . shfmt-on-save-mode)
  :custom
  (shfmt-arguments '("-i" "4")))

(use-package sh-script
  :straight nil
  :init
  (when (boremacs/executable-find "bash-language-server")
    (add-hook 'sh-mode-hook #'boremacs/eglot-lazy-ensure))
  :custom
  (sh-indent-after-continuation 'always)
  :config
  (boremacs/executable-find "shellcheck")
  (add-to-list 'sh-imenu-generic-expression
               '(sh (nil "^\\s-*function\\s-+\\([[:alpha:]_-][[:alnum:]_-]*\\)\\s-*\\(?:()\\)?" 1)
                    (nil "^\\s-*\\([[:alpha:]_-][[:alnum:]_-]*\\)\\s-*()" 1))))

(provide 'init-bash)
;;; init-bash.el ends here
