;;; init-dired.el --- Dired customisations -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:


(use-package dired
  :straight (dired :type built-in)
  :config
  (setq delete-by-moving-to-trash t)
  :custom
  (dired-recursive-deletes 'top)
  (dired-dwim-target t)
  :bind
  (:map dired-mode-map
        ("<mouse-2>" . dired-fine-file)
        ("C-c C-q" . wdired-change-to-wdired-mode)
        :map ctl-x-map
        ("C-j" . dired-jump)
        ("C-j" . dired-jump-other-window)))

(use-package dired-x
  :after direx
  :straight nil
  :config
  (setq dired-omit-files
        (concat dired-omit-files "\\|^\\..*$")))

(use-package diredfl
  :after dired
  :hook (dired-mode . diredfl-mode))

(use-package dired-collapse
  :after dired
  :hook (dired-mode . dired-collapse-mode))

(use-package dirvish
  :after dired
  :custom
  (dirvish-menu-bookmarks
   '(("h" "~/" "Home")
     ("d" "~/Downloads/" "Downloads")))
  (dirvish-mode-line-format ; it's ok to place string inside
   '(:left (sort file-time " " file-size symlink) :right (omit yank index)))
  (dirvish-attributes
      '(vc-state subtree-state nerd-icons collapse git-msg file-time file-size))
  (dirvish-subtree-state-style 'nerd)
  (dirvish-side-attributes (append '(vc-state git-msg) dirvish-attributes))
  (dirvish-side-preview-dispatchers (append '(vc-diff) dirvish-preview-dispatchers))
  (dirvish-side-mode-line-format '(:left (vc-info sort filter) :right (index)))
  :init
  (dirvish-override-dired-mode)
  (dirvish-peek-mode)
  :bind
  ;; Bind `dirvish|dirvish-dired|dirvish-side|dirvish-dwim' as you see fit
  (("C-c f" . dirvish-fd)
   ("M-0" . dirvish-side)
   :map dired-mode-map
   ("TAB" . dirvish-toggle-subtree)
   ("SPC" . dirvish-show-history)
   ("*"   . dirvish-mark-menu)
   ("r"   . dirvish-roam)
   ("b"   . dirvish-goto-bookmark)
   ("f"   . dirvish-file-info-menu)
   ("M-n" . dirvish-go-forward-history)
   ("M-p" . dirvish-go-backward-history)
   ("M-s" . dirvish-setup-menu)
   ("M-f" . dirvish-toggle-fullscreen)
   ([remap dired-sort-toggle-or-edit] . dirvish-quicksort)
   ([remap dired-do-redisplay] . dirvish-ls-switches-menu)
   ([remap dired-summary] . dirvish-dispatch)
   ([remap dired-do-copy] . dirvish-yank-menu)
   ([remap mode-line-other-buffer] . dirvish-other-buffer)))

(provide 'init-dired)
;;; init-dired.el ends here
