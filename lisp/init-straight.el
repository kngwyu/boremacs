;;; init-straight.el --- Bootstrap straight package manager -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:


(defvar bootstrap-version)

(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/master/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

(setq straight-use-package-by-default t
      straight-check-for-modifications (list 'check-at-save 'find-when-checking)
      straight-enable-package-integration nil)

(when boremacs/debug
  (setq use-package-verbose 'debug))

(provide 'init-straight)
;;; init-utils.el ends here
