;;; init-dockerfile.el --- Support for Dockerfile -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package dockerfile-mode
  :mode "\\.def\\'"
  :diminish)

(provide 'init-dockerfile)
;;; init-dockerfile.el ends here
