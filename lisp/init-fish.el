;;; init-fish.el --- Support for writing fish shell scripts -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:


(use-package fish-mode
  :mode ("\\.fish\\'" . fish-mode)
  :hook (before-save-hook . fish_indent-before-save))

(provide 'init-fish)
;;; init-fish.el ends here
