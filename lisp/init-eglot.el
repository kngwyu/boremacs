;;; init-eglot.el --- LSP support via eglot          -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(require 'cl-lib)

(setq read-process-output-max (* 1024 1024))

(defun boremacs/eglot-lazy-ensure ()
  "To prevent dirvish preview from starting eglot."
  (run-at-time 3 nil #'eglot-ensure))

(use-package jsonrpc)

(use-package eglot
  :commands eglot eglot-ensure
  :straight `(eglot :type
                    ,(if (version< emacs-version "29.0") 'git 'built-in))
  :custom
  (eglot-connect-timeout 10)
  (eglot-autoshutdown t)
  (eglot-autoreconnect nil)
  (eglot-confirm-server-initiated-edits nil) ;; Don't ask confirmation
  :config
  (add-to-list 'eglot-server-programs
               '(astro-mode . ("astro-ls" "--stdio"
                               :initializationOptions
                               (:typescript (:tsdk "./node_modules/typescript/lib"))))))

(use-package eglot-booster
  :when (boremacs/executable-find "emacs-lsp-booster")
  :straight
  (eglot-booster :host github
                 :repo "jdtsmith/eglot-booster")
  :after eglot
  :config (eglot-booster-mode))

(use-package consult-eglot
  :after consult eglot
  :bind
  (:map eglot-mode-map
        ([remap xref-find-apropos] . consult-eglot-symbols)))

(provide 'init-eglot)
;;; init-eglot.el ends here
