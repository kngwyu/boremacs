;;; init.el --- Load the full configuration -*- lexical-binding: t -*-
;;; Commentary:

;; This file bootstraps the configuration, which is divided into
;; a number of other files.

;;; Code:


;; Set t to show package loading time
(defvar boremacs/debug nil)


;; Produce backtraces when errors occur: can be helpful to diagnose startup issues
(setq debug-on-error boremacs/debug)

;; Set user-emacs-directory
(let ((env-user-emacs-dir (getenv "USER_EMACS_DIRECTORY")))
  (unless (eq env-user-emacs-dir nil)
    (setq user-emacs-directory env-user-emacs-dir)))

;; Startup optimization
(let ((normal-gc-cons-threshold (* 20 1024 1024))
      (init-gc-cons-threshold (* 128 1024 1024)))
  (setq gc-cons-threshold init-gc-cons-threshold)
  (add-hook 'emacs-startup-hook
            (lambda () (setq gc-cons-threshold normal-gc-cons-threshold))))

;; Networking configs, borrowed from https://github.com/radian-software/radian/
;; Feature `gnutls' provides support for SSL/TLS connections, using
;; the GnuTLS library.
(with-eval-after-load 'gnutls
  (eval-when-compile (require 'gnutls))
  ;; Do not allow insecure TLS connections.
  (setq gnutls-verify-error t)
  ;; Bump the required security level for TLS to an acceptably modern
  ;; value.
  (setq gnutls-min-prime-bits 3072))

;; Feature `url-http' is a library for making HTTP requests.
(with-eval-after-load 'url-http
  (eval-when-compile (require 'url-http))
  (defun boremacs/+no-query-on-http-kill (buffer)
    "Disable query-on-exit for all network connections.
This prevents Emacs shutdown from being interrupted just because
there is a pending network request."
    (prog1 buffer
      (set-process-query-on-exit-flag
       (get-buffer-process buffer) nil)))
  (advice-add #'boremacs/+no-query-on-http-kill :filter-return #'url-http))


;; Load path
(push (expand-file-name "lisp" user-emacs-directory) load-path)
;; custom file
(setq custom-file (locate-user-emacs-file "custom.el"))

;; Bootstrap straight to install packages
(require 'init-straight)
;; Some useful functions
(require 'init-utils)
;; Some random config
(require 'init-misc)
;; Set up $PATH
(require 'init-exec-path)
;; WSL specific setting
(require 'init-wsl)
;; Frame settings
(require 'init-frame-hooks)
(require 'init-xterm)
(require 'init-gui-frames)
;; Buffer
(require 'init-uniquify)
(require 'init-ibuffer)
(require 'init-minibuffer-completion)
(require 'init-isearch)
(require 'init-windows)
;; Code editing
(require 'init-editing-utils)
(require 'init-whitespace)

;; Additional settings are only loaded when BOREMACS_MINIMAL is nil
(when (eq (getenv "BOREMACS_MINIMAL") nil)
  ;; Theme
  (require 'init-themes)
  ;; Dired
  (require 'init-dired)
  ;; Japanese
  (require 'init-migemo)
  (require 'init-skk)
  ;; Session
  (require 'init-recentf)
  (require 'init-sessions)
  ;; Code completion
  (require 'init-code-completion)
  (require 'init-flymake)
  (require 'init-eglot)
  ;; Template
  (require 'init-template)
  ;; Version control
  (require 'init-vc)
  ;; Compile
  (require 'init-compile)
  ;; Ellama
  (require 'init-ellama)
  ;; Programming Languages
  (require 'init-bash)
  (require 'init-dockerfile)
  (require 'init-fish)
  (require 'init-javascript)
  (require 'init-lisp)
  (require 'init-lua)
  (require 'init-paredit)
  (require 'init-php)
  (require 'init-python)
  (require 'init-racket)
  (require 'init-rust)
  (require 'init-sql)
  (require 'init-web)
  ;; Data formats
  (use-package json-mode :mode "\\.js\\(?:on\\|[hl]int\\(?:rc\\)?\\)\\'")
  (use-package toml-mode :mode "\\.toml'")
  (use-package yaml-mode :mode "\\.yml'")
  ;; Documents
  (require 'init-org)
  (require 'init-latex)
  (require 'init-markdown)
  (require 'init-spelling)
  ;; Keychain
  (require 'init-keychain))
;; Local Variables:
;; coding: utf-8
;; no-byte-compile: t
;; End:
;;; init.el ends here
(put 'set-goal-column 'disabled nil)
