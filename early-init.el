;;; early-init.el --- Emacs 27+ pre-initialisation config

;;; Commentary:

;; I don't use package.el
;;; Code:

(setq package-enable-at-startup nil     ; don't auto-initialize!
      ;; don't add that `custom-set-variables' block to my init.el!
      package--init-file-ensured t)

(provide 'early-init)

;;; early-init.el ends here
